# Generated by Django 3.2.7 on 2021-11-04 16:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('threader', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='thread',
            name='date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
