# Generated by Django 3.2.7 on 2021-11-05 09:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('threader', '0005_rename_thread_thread_content'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thread',
            name='image',
            field=models.FileField(blank=True, null=True, upload_to='images'),
        ),
    ]
